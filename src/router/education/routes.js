import Layout from '@/views/layout/Layout'
import curso from '@/modules/curso/routes'

export default {
  path: '/educacao',
  component: Layout,
  redirect: '/educacao/curso',
  name: 'Educacao',
  meta: { title: 'Educacao', icon: 'user' },
  children: [
    ...curso
  ]
}
