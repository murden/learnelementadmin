import Layout from '@/views/layout/Layout'
import pais from '@/modules/pais/routes'
import uf from '@/modules/uf/routes'
import cidade from '@/modules/cidade/routes'
import sede from '@/modules/sede/routes'
import regiao from '@/modules/regiao/routes'
import microRegiao from '@/modules/microRegiao/routes'
//
import cidade2 from '@/modules/cidade2/routes'
//
export default {
  path: '/localizacao',
  component: Layout,
  redirect: '/localizacao/cidade',
  name: 'Local',
  meta: { title: 'Local', icon: 'user' },
  children: [
    ...pais,
    ...uf,
    ...cidade,
    //
    ...cidade2,
    ...sede,
    ...regiao,
    ...microRegiao
  ]
}
