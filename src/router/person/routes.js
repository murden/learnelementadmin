import Layout from '@/views/layout/Layout'
import pessoa from '@/modules/pessoa/routes'
import cidade from '@/modules/cidade/routes'
export default {
  path: '/pessoa',
  component: Layout,
  redirect: '/pessoa',
  name: 'Pessoa',
  meta: { title: 'Pessoa', icon: 'user' },
  children: [
    ...pessoa,
    ...cidade
  ]
}
