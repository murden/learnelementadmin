import axios from 'axios'
const apiUrl = 'https://viacep.com.br/ws/'

class CityService {
  constructor() {
    this.axios = axios
    this.api = apiUrl
  }

  get(cep) {
    return this.axios.get(`${this.api}${cep}/json/`)
  }
}

export default new CityService()
