import Service from '@/modules/_common/api/BaseService'

export default class PessoaService extends Service {
  constructor() {
    super('/pessoa')
  }
  findCoordenadores() {
    return this.axios.get(`${process.env.BASE_API}/coordenador`)
  }
  findVendedores(coordenador) {
    let query = ''
    if (coordenador) {
      query = `coordenador=${coordenador}`
    }
    return this.axios.get(`${process.env.BASE_API}/vendedor?${query}`)
  }
}
