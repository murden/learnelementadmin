import { CRUD } from '@/modules/_common/store/crud'

import PessoaService from './service'
const service = new PessoaService()
const crud = CRUD(service)

const module = {
  namespaced: true,
  modules: {
    crud
  },
  state: {
    vendedor: {},
    coordenador: {},
    vendedores: [],
    coordenadores: []
  },
  getters: {
    vendedores: (state) => state.vendedores,
    coordenadores: (state) => state.coordenadores,
    vendedor: (state) => state.vendedor,
    coordenador: (state) => state.coordenador
  },
  mutations: {
    SET_COORDENADORES(state, payload) {
      state.coordenadores = payload
    },
    SET_VENDEDORES(state, payload) {
      state.vendedores = payload
    },
    SET_VENDEDOR(state, payload) {
      state.vendedor = payload
    },
    SET_COORDENADOR(state, payload) {
      state.coordenador = payload
    }
  },
  actions: {
    /**
     *  Enviar requisição para adicionar um novo item na base
     * @param {*} context
     * @param {T} payload
     */
    createAdmin(context, payload) {
      context.commit('SET_LOADING', true)
      return service.post('admin', payload)
        .then(() => context.dispatch('findPaginated', {}))
        .finally(() => context.commit('SET_LOADING', false))
    },

    findCoordenadores(context, payload) {
      context.commit('SET_LOADING', true)
      return service.findCoordenadores() // service.post('admin', payload)
        .then((response) => context.commit('SET_COORDENADORES', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    },
    findVendedores(context, payload) {
      context.commit('SET_LOADING', true)
      return service.findVendedores(payload) // service.post('admin', payload)
        .then((response) => context.commit('SET_VENDEDORES', response.data))
        .finally(() => context.commit('SET_LOADING', false))
    }
  }
}

export default module
